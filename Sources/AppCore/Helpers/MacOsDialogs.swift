import Foundation
import AppKit

public class MacOsDialogs {
    public static func open(title: String, conf: Set<MacOsDlgSettings>, startUrl: URL?, buttonText: String? = nil) -> URL? {
        let dialog = NSOpenPanel().then {
            $0.title                   = title
            $0.message                 = title
            $0.canHide = false
            $0.showsResizeIndicator    = true
            $0.canCreateDirectories    = true
            
            if let startUrl = startUrl {
                $0.directoryURL = startUrl
            }
            
            $0.canChooseDirectories    = conf.contains(.canChooseDirs)
            $0.canChooseFiles          = conf.contains(.canChooseFiles)
            $0.allowsMultipleSelection = conf.contains(.multipleSelection)
            
            $0.showsHiddenFiles        = conf.contains(.showsHiddenFiles)
            
            if let buttonText = buttonText {
                $0.prompt = buttonText
            }
        }
        
        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            return dialog.url
        }
        
        return nil
    }
    
    public static func save(title: String, startDir: URL, fileName: String, _ conf: Set<MacOsSaveDlgSettings> ) -> URL?  {
        let dialog = NSSavePanel().then {
            $0.title                   = title
            $0.message                 = title
            $0.showsResizeIndicator    = true
            $0.canCreateDirectories    = true
            
            $0.showsHiddenFiles        = conf.contains(.showsHiddenFiles)
            
            $0.directoryURL = startDir
            $0.nameFieldStringValue = fileName
        }
        
        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            return dialog.url
        }
        
        return nil
    }
}

public enum MacOsDlgSettings {
    case canChooseDirs
    case canChooseFiles
    case multipleSelection
    case showsHiddenFiles
}

public enum MacOsSaveDlgSettings {
    case showsHiddenFiles
}
