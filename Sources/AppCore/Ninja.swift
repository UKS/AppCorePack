import Foundation
import AsyncNinja

public class NinjaContext {
    open class Main : ExecutionContext, ReleasePoolOwner {
        public var executor    = Executor.init(queue: DispatchQueue.main)
        public let releasePool = ReleasePool()
        
        public init() {}
    }
    
    open class Global : ExecutionContext, ReleasePoolOwner {
        public var executor    = Executor.init(queue: DispatchQueue.global(qos: .userInteractive))
        public let releasePool = ReleasePool()
        
        public init() {}
    }
}
