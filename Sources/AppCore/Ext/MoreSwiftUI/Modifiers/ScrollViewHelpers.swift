import Foundation
import SwiftUI

///To apply you need to code for content: .background(ScrollViewContentTopLeftFix())
@available(OSX 11.0, *)
public struct ScrollViewContentTopLeftFix: NSViewRepresentable {
    public init(){}
    
    public func makeNSView(context: NSViewRepresentableContext<ScrollViewContentTopLeftFix>) -> NSView {
        let view = NSView(frame: .zero )
        DispatchQueue.main.async { // << must be async, so view got into view hierarchy
            view.enclosingScrollView?.contentView.scroll(to: .zero)
            view.enclosingScrollView?.reflectScrolledClipView(view.enclosingScrollView!.contentView)
        }
        return view
    }

    public func updateNSView(_ nsView: NSView, context: NSViewRepresentableContext<ScrollViewContentTopLeftFix>) {
    }
}
