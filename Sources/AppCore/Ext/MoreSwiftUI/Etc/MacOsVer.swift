//
//  MacOsVer.swift
//  AppCore
//
//  Created by UKS on 31.10.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import Foundation

public class MacOsVer {
    public static var higherThan_11: Bool {
        if #available(macOS 11.0, *) { return true }
        else { return false }
    }
    
    public static var higherThan_11_3: Bool {
        if #available(macOS 11.3, *) { return true }
        else { return false }
    }
    
    public static var higherThan_12: Bool {
        if #available(macOS 12, *) { return true }
        else { return false }
    }
}
