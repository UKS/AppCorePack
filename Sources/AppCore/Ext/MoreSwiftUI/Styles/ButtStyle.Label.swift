import Foundation
import SwiftUI

public extension ButtStyle {
    @available(macOS 11.0, *)
    struct Label: ButtonStyle {
        let color : Color?
        
        public init(color: Color? = nil) {
            self.color = color
        }
        
        public func makeBody(configuration: Configuration) -> some View {
            LabelButtView(configuration: configuration, color: color)
        }
    }
}

@available(macOS 11.0, *)
struct LabelButtView : View {
    let configuration: ButtonStyle.Configuration
    let color : Color?
    
    @State var hover : Bool = false
    
    var body: some View {
        ZStack {
            configuration.label
                .theme(.normal(hover ? (configuration.isPressed ? .lvl_7 : .lvl_8) : .lvl_6))
                .animation(.default, value: hover)
                .animation(.default, value: configuration.isPressed)
            
            if let color = color {
                configuration.label
                    .foregroundColor(color)
                    .opacity(0.3)
            }
                
        }.onHover { hover = $0 }
    }
}
