import Foundation

public func waitForCallback(code: @escaping ( @escaping ()->Void )->Void) {
    let DISPATCH_GROUP = DispatchGroup()
    DISPATCH_GROUP.enter()
    
    code {
        DISPATCH_GROUP.leave()
    }
    
    DISPATCH_GROUP.wait()
}
