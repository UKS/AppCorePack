import SwiftUI

@available(OSX 11.0, *)
public struct LoadingView : View {
    public init(){}
    let baseAnimation = Animation.linear(duration: 1)
    @State var isTransparent = true
    
    public var body: some View {
        ProgressView()
            .progressViewStyle(CircularProgressViewStyle())
            .opacity(isTransparent ? 0 : 1)
            .animation(.default, value: false)
            .animation(baseAnimation, value: isTransparent)
            .onAppear() {
                isTransparent = false
            }
    }
}

@available(OSX 11.0, *)
struct LockedUIView: View {
    let baseAnimation = Animation.linear(duration: 1)
    @State var isTransparent = true
    
    var body: some View {
        RoundedRectangle(cornerRadius: 4)
            .fill(Color.black)
            .blur(radius: 7.0)
            .opacity(isTransparent ? 0 : 0.3)
            .overlay( LoadingView() )
            // Disable clicks through the locking view
            .onTapGesture { }
            .animation(baseAnimation, value: isTransparent)
            .onAppear() {
                isTransparent = false
            }
    }
}


@available(OSX 11.0, *)
public extension View {
    @ViewBuilder
    func loadingMod( inProgress: Bool ) -> some View {
        ZStack {
            self
                .disabled(inProgress)
                .animation(Animation.linear(duration: 1), value: inProgress)
                .blur(radius: inProgress ? 5 : 0)
            
            if inProgress {
                LoadingView()
            }
        }
    }
    
    
    func loadingModLockUi() -> some View {
        self.overlay( LockedUIView() )
    }
}
