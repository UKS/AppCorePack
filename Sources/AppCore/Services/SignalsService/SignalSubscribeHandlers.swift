//
//  SignalSubscribeHandlers.swift
//  AppCore
//
//  Created by Loki on 9/13/19.
//  Copyright © 2019 Loki. All rights reserved.
//

import Foundation

func initSignalSubscribeHandlers() {
    initDateChange()
}

func initDateChange() {
    var lastDay = Time().dayOf(date: Date())
    
    SignalsService.main.onSubscribe(Signal.DateDidChange.self)
        .takeOne()
        .onUpdate {
            TimerZbs.start(id: "DateDidChange", interval: 1) {
                let today = Time().dayOf(date: Date())

                if lastDay != today {
                    SignalsService.main.send(signal: Signal.DateDidChange(from: lastDay, to: today))
                    lastDay = today
                }
            }
    }
    
    if #available(OSX 10.15, *) {
        SignalsService.main.onSubscribe(Signal.ColorThemeDidChange.self)
            .takeOne()
            .onUpdate { _ in
                
                DistributedNotificationCenter.default
                    .publisher(for: NSNotification.Name(rawValue: "AppleInterfaceThemeChangedNotification"))
                    .asyncNinja
                    .onUpdate(executor: .main) { _ in
                        AppCore.signals.send(signal: Signal.ColorThemeDidChange())
                }
                
                DispatchQueue.main.async {
                    AppCore.signals.send(signal: Signal.ColorThemeDidChange())
                }
        }
    }
}
