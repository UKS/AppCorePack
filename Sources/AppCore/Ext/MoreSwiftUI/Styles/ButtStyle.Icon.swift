import Foundation
import SwiftUI

public extension ButtStyle {
    @available(macOS 11.0, *)
    struct Icon: ButtonStyle {
        let id : String
        let color : Color?
        
        public init(id: String, color: Color? = nil) {
            self.id = id
            self.color = color
        }
        
        public func makeBody(configuration: Configuration) -> some View {
            IconButtonView(id: id, configuration: configuration, color: color)
        }
    }
}

@available(macOS 11.0, *)
struct IconButtonView : View {
    let id : String
    let configuration: ButtonStyle.Configuration
    let color : Color?
    
    @State var hover : Bool = false
    
    var body: some View {
        ZStack {
            Image(systemName: id)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .theme(.normal(hover ? (configuration.isPressed ? .lvl_7 : .lvl_8) : .lvl_5))
                .animation(.default, value: hover)
                .animation(.default, value: configuration.isPressed)
            
            if let color = color {
                Image(systemName: id)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(color)
                    .opacity(0.3)
            }
                
        }.onHover { hover = $0 }
    }
}
