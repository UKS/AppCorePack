import SwiftUI

@available(macOS 10.15, *)
public extension View {
    func animationDisable() -> some View {
        self.animation(nil, value: UUID())
    }
}
