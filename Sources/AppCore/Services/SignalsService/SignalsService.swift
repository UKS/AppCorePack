import Foundation
import AsyncNinja

final public class SignalsService {
    public static var main : SignalsService = { return SignalsService() }()
    var dispatchers = [Int:Any]()
    var subscribeNotifiers = [Int:Producer<Void,Void>]()
    
    public init() {}
    
    public func send<Signal>(signal: Signal) {
        let dispatcher = getDispatcher(type(of: signal))
        dispatcher.send(signal)
    }
    
    public func subscribeFor<Signal>(_ signalType: Signal.Type) -> Producer<Signal,Void> {
        subscribeNotifier(signalType)?.update(())
        return getDispatcher(signalType).dispatcher
    }
    
    public func onSubscribe<Signal>(_ signalType: Signal.Type) -> Producer<Void,Void> {
        if let producer = subscribeNotifier(signalType) {
            return producer
        } else {
            let hash = ObjectIdentifier(Signal.self).hashValue
            subscribeNotifiers[hash] = Producer<Void,Void>()
            return subscribeNotifiers[hash]!
        }
    }
}

private extension SignalsService {
    func getDispatcher<Signal>(_ signalType: Signal.Type) -> SignalDispatcher<Signal> {
        let hash = ObjectIdentifier(Signal.self).hashValue
        if let dispatcher = dispatchers[hash] as? SignalDispatcher<Signal> {
            return dispatcher
        } else {
            let dispatcher = SignalDispatcher<Signal>()
            dispatchers[hash] = dispatcher
            return dispatcher
        }
    }
    
    func subscribeNotifier<Signal>(_ signalType: Signal.Type) -> Producer<Void,Void>? {
        let hash = ObjectIdentifier(Signal.self).hashValue
        return subscribeNotifiers[hash]
    }
}
