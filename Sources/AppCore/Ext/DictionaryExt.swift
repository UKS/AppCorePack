import Foundation

public extension Dictionary {
    mutating func verifyExistanceOf(key: Key, defaultValue: Value) -> Value {
        if let val = self[key] {
            return val
        } else {
            self[key] = defaultValue
            return defaultValue
        }
    }
}

public extension Dictionary where Value == Int {
    mutating func incrementIntFor(key: Key) {
        if self[key] == nil {
            self[key] = 0
        }
        
        self[key]! += 1
    }
}
