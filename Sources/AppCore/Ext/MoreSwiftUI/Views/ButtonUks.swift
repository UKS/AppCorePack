import Foundation
import SwiftUI

@available(macOS 11.00, *)
public struct ButtonUks: View {
    var text: String
    @State var hovering: Bool = false
    var action: ()->()
    
    public init(_ text: String, action: @escaping ()->() ) {
        self.text = text
        self.action = action
    }
    
    public var body: some View {
        Text(text)
            .opacity( hovering ? 0.7 : 1 )
            .onHover( perform: { self.hovering = $0 } )
            .onTapGesture { action() }
    }
}
