import Foundation

public enum AppCoreError : Swift.Error {
    case generic(info: String)
}

extension AppCoreError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .generic(let info):
            return "Error: \(info)"
        }
    }
}
