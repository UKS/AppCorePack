import Foundation
import SwiftUI

@available(macOS 10.15, *)
public extension View {
    func modify<T: View>(@ViewBuilder _ modifier: (Self) -> T) -> some View {
        return modifier(self)
    }
}
