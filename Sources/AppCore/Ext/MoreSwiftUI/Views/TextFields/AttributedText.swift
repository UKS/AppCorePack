import SwiftUI
import Cocoa

@available(OSX 11.0, *)
public struct AttributedText: View {
    @Binding var text: NSAttributedString
    
    public init(attributedString: Binding<NSAttributedString>) {
        _text = attributedString
    }
    
    public var body: some View {
        AttributedTextInternal(attributedString: $text)
            .frame(minWidth: $text.wrappedValue.size().width + 350, minHeight: $text.wrappedValue.size().height )
    }
}

@available(OSX 11.0, *)
public struct AttributedTextInternal: NSViewRepresentable {
    @Binding var text: NSAttributedString
    
    public init(attributedString: Binding<NSAttributedString>) {
        _text = attributedString
    }
    
    public func makeNSView(context: Context) -> NSTextView {
        let textView = NSTextView()
        textView.isRichText = true
        textView.isSelectable = true
        
        textView.setContent(text: text, makeNotEditable: true)
        
        return textView
    }
    
    public func updateNSView(_ textView: NSTextView, context: Context) {
        textView.setContent(text: text, makeNotEditable: true)
    }
}

extension NSTextView {
    func setContent(text: NSAttributedString, makeNotEditable: Bool) {
        self.isEditable = true
        
        self.selectAll(nil)
        self.insertText(text, replacementRange: self.selectedRange())
        
        self.isEditable = !makeNotEditable
    }
}
