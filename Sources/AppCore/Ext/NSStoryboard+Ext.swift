import Foundation
import AppKit

public extension NSStoryboard {
    func windowController<T>(id: String) -> T where T : NSWindowController {
        return instanciateConroller(storyboardId: id) as! T
    }
    
    func viewController<T>(id: String) -> T where T : NSViewController {
        return instanciateConroller(storyboardId: id) as! T
    }
    
    func instanciateConroller(storyboardId: String) -> Any {
        return instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(storyboardId))
    }
}
