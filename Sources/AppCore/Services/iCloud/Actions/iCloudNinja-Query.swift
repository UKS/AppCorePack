import CloudKit
import AsyncNinja

public extension CKDatabase {
    func query(operation: CKQueryOperation) -> Channel<CKRecord, CKQueryOperation?> {
        return producer() { [weak self] producer in
            
            operation.recordFetchedBlock = {
                producer.update($0)
            }
            
            operation.queryCompletionBlock = { cursor, error in
                if let error = error {
                    producer.fail(error)
                }
                if let cursor = cursor {
                    producer.succeed(CKQueryOperation(cursor: cursor))
                } else {
                    producer.succeed(nil)
                }
            }
            
            self?.add(operation)
        }
    }
}

fileprivate func log(error: Error) {
    AppCore.log(title: "iCloudNinja", error: error)
}

fileprivate func log(msg: String) {
    AppCore.log(title: "iCloudNinja", msg: msg, thread: true)
}
