
import Foundation

public enum InternetProtocol: RawRepresentable {
    case ssh, https
  
    public init?(rawValue: String) {
        switch rawValue {
        case String(kSecAttrProtocolSSH):
          self = .ssh
        case String(kSecAttrProtocolHTTPS):
          self = .https
        default: return nil
        }
    }
    
    public var rawValue: String {
        switch self {
        case .ssh:
            return String(kSecAttrProtocolSSH)
        case .https:
            return String(kSecAttrProtocolHTTPS)
        }
    }
}
