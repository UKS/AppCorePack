import SwiftUI

@available(OSX 11.0, *)
public struct ProgressLine: View {
    @Binding var progressRatio: CGFloat
    
    public init (progressRatio: Binding<CGFloat>) {
        _progressRatio = progressRatio
    }
    
    public var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                Rectangle().frame(width: geometry.size.width , height: geometry.size.height)
                    .opacity(0.3)
                    .foregroundColor(Color.gray)
                
                Rectangle().frame(width: min(progressRatio * geometry.size.width, geometry.size.width), height: geometry.size.height)
                    .foregroundColor( Color.blue )
                    .animation(.linear)
            }.cornerRadius(45.0)
        }
    }
}
