import AppKit

public extension NSViewController {
    func childBy<T>(type: T.Type) -> T {
        return children.first(where: { $0 is T })  as! T
    }
}
