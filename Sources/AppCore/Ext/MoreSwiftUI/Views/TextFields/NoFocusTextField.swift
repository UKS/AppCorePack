import SwiftUI
import Cocoa

@available(OSX 11.0, *)
public struct NoFocusTextField: NSViewRepresentable {
    @Binding var text: String

    public init(text: Binding<String>) {
        _text = text
    }

    public func makeNSView(context: Context) -> NSTextField {
        let textField = NSTextField(string: text)
        textField.delegate = context.coordinator
        textField.isBordered = false
        textField.backgroundColor = nil
        textField.focusRingType = .none
        return textField
    }

    public func updateNSView(_ nsView: NSTextField, context: Context) {
        nsView.stringValue = text
    }

    public func makeCoordinator() -> Coordinator {
        Coordinator { self.text = $0 }
    }
}

public final class Coordinator: NSObject, NSTextFieldDelegate {
    var setter: (String) -> Void

    init(_ setter: @escaping (String) -> Void) {
        self.setter = setter
    }

    public func controlTextDidChange(_ obj: Notification) {
        if let textField = obj.object as? NSTextField {
            setter(textField.stringValue)
        }
    }
}



extension NSTextField {
    func bestHeight(for text: String, width: CGFloat) -> CGFloat {
        stringValue = text
        let height = cell!.cellSize(forBounds: NSRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude)).height

        return height
    }

    func bestWidth(for text: String, height: CGFloat) -> CGFloat {
        stringValue = text
        let width = cell!.cellSize(forBounds: NSRect(x: 0, y: 0, width: .greatestFiniteMagnitude, height: height)).width

        return width
    }
    
    func bla() -> CGFloat {
        self.font!.pointSize
    }
}
