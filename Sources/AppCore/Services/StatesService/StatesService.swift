import Foundation
import AsyncNinja

public class StatesService {
    private var states = [AnyHashable:Any]()
    
    public func set<ValueType>(value: ValueType, forKey key: AnyHashable) {
        #if DEBUG
//        if Mirror(reflecting: value).displayStyle == .class {
//            fatalError("StateMonitorService accepts only structs as value. Can't accept class")
//        }
        #endif
        
        if let item = getItem(key: key) as StatesServiceItem<ValueType>? {
            item.value = value
        } else {
            states[key] = StatesServiceItem(value)
        }
    }
    
    public func notify<ValueType>(value: ValueType.Type, key: AnyHashable) {
        if let item = getItem(key: key) as StatesServiceItem<ValueType>? {
            if let value = item.value {
                item.didChange.update(value)
            }
        }
    }
    
    public func valueFor<ValueType>(key: AnyHashable) -> ValueType? {
        return getItem(key: key)?.value
    }
    
    public func subscribeFor<ValueType>(key: AnyHashable, valueOfType: ValueType.Type) -> Channel<ValueType,Void> {
        if let item = getItem(key: key) as StatesServiceItem<ValueType>? {
            return item.didChange
        } else {
            states[key] = StatesServiceItem<ValueType>()
            return getItem(key: key)!.didChange
        }
    }
}

public extension StatesService { // type as key
    func set<KeyType, ValueType>(value: ValueType, forKey key: KeyType.Type) {
        let hash = ObjectIdentifier(key).hashValue
        set(value: value, forKey: hash)
    }
    
    func valueFor<KeyType, ValueType>(key: KeyType.Type) -> ValueType? {
        let hash = ObjectIdentifier(key).hashValue
        return getItem(key: hash)?.value
    }
    
    func subscribeFor<KeyType, ValueType>(key: KeyType.Type, type: ValueType.Type) -> Channel<ValueType,Void> {
        let hash = ObjectIdentifier(key).hashValue
        return subscribeFor(key: hash, valueOfType: type)
    }
}

private extension StatesService {
    func getItem<ValueType>(key: AnyHashable) -> StatesServiceItem<ValueType>? {
        if let dicItem = states[key] {
            guard let item = dicItem as? StatesServiceItem<ValueType> else {
                let msg = "ERROR - key \(key) is already bound to type \(type(of:dicItem)). Can't bind to \(ValueType.self)"
                AppCore.log(title: "StateMonitorService", msg: msg)
                assert(false)
                return nil
            }
            
            return item
        }
        
        return nil
    }
}
