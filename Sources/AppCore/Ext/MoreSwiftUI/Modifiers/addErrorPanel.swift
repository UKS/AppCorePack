import SwiftUI

@available(OSX 11.0, *)
public extension View {
    @ViewBuilder
    func addErrorPanel(error: Binding<Error?>) -> some View {
        if error.wrappedValue != nil {
            ZStack {
                self
                
                ErrorPanelView(error: error)
                    .animation(.easeInOut)
            }
        } else {
            self
        }
    }
}

@available(macOS 11.0, *)
public struct ErrorPanelView : View {
    @Environment(\.colorScheme) private var colorScheme: ColorScheme
    
    @Binding var error: Error?
    var errorStr: String { error?.detailedDescription ??  "" }
    
    public var body: some View {
        ZStack{
            //blured background
            VStack{
                Space()
                HStack(alignment: .top) {
                    Text("Error: \(errorStr)")
                        .foregroundColor( colorScheme == .dark ? .yellow : .gray )
                    Space()
                    Button("x"){}.background( Color.clear )
                }
                .frame(minHeight:10)
                .padding(10)
                .background(Color(rgbaHex: 0xcc0000aa))
                .cornerRadius(8)
                .blur(radius: 4)
            }
            // End
            
            //information part
            VStack {
                Space()
                HStack(alignment: .top) {
                    Text("Error: \(errorStr)")
                        .contextMenu{
                            Button("Copy") {
                                NSPasteboard.general.clearContents()
                                NSPasteboard.general.setString( "\(errorStr)" , forType: NSPasteboard.PasteboardType.string)
                            }
                        }
                    
                    Space()
                    Button("x"){
                        self.error = nil
                    }
                }
                .frame(minHeight:10)
                .padding(10)
            }
        }
        .opacity( (error == nil) ? 0 : 1)
        .animation(.default)
    }
}
