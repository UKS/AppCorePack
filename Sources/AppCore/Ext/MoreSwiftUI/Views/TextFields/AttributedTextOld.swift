import SwiftUI
import Cocoa

@available(OSX 11.0, *)
public struct AttributedTextOld: NSViewRepresentable {
    @Binding var text: NSAttributedString
    private let selectable: Bool
    
    public init(attributedString: Binding<NSAttributedString>, selectable: Bool = true) {
        _text = attributedString
        self.selectable = selectable
    }
    
    public func makeNSView(context: Context) -> NSTextField {
        let textField = NSTextField(labelWithAttributedString: text)
        textField.preferredMaxLayoutWidth = textField.frame.width
        textField.allowsEditingTextAttributes = true // Fix of clear of styles on click
        
        textField.isSelectable = selectable
        
        return textField
    }
    
    public func updateNSView(_ textField: NSTextField, context: Context) {
        textField.attributedStringValue = $text.wrappedValue
    }
}
