//
//  Logger.swift
//  TaoGit
//
//  Created by UKS on 12.11.2019.
//  Copyright © 2019 Cheka Zuja. All rights reserved.
//

public protocol Logger {}

public extension Logger {
    func log(msg: String, thread: Bool = false) {
        AppCore.log(title: String(describing: type(of: self)), msg: msg, thread: thread)
    }
    
    func logAdvanced(msg: String,
             thread: Bool = false,
             fileName: String = #file,
             lineNumber: Int = #line,
             functionName: String = #function) {
        
        let debugInfo = "file:\(fileName)|line:\(lineNumber)|func:\(functionName)|Msg: \(msg)"
        let type = String(describing: type(of: self))
        
        var suffix = ""
        
        if let loggerID = self as? LoggerID {
            suffix = "+" + loggerID.loggerID
        }
        AppCore.log(title: type + suffix , msg: debugInfo, thread: thread)
        
    }

    func log(error: Error, thread: Bool = false) {
        let type = String(describing: type(of: self))
        var suffix = ""
        
        if let loggerID = self as? LoggerID {
            suffix = "+" + loggerID.loggerID
        }
        
        AppCore.log(title: type + suffix, error: error, thread: thread)
    }
}

public protocol LoggerID : Logger {
    var loggerID : String { get }
}
