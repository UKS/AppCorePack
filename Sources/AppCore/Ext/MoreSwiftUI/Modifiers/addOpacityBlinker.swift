import SwiftUI
import Combine

@available(OSX 11.0, *)
public extension View {
    // the generic constraints here tell the compiler to accept any publisher
    //   that sends outputs no value and never errors
    // this could be a PassthroughSubject like above, or we could even set up a TimerPublisher
    //   that publishes on an interval, if we wanted a looping animation
    //   (we'd have to map it's output to Void first)
    func addOpacityBlinker<T: Publisher>(subscribedTo publisher: T, duration: Double = 1, power: CGFloat = 5)
        -> some View where T.Output == Void, T.Failure == Never {
            
            // here I take whatever publisher we got and type erase it to AnyPublisher
            //   that just simplifies the type so I don't have to add extra generics below
            self.modifier(OpacityBlinker(subscribedTo: publisher.eraseToAnyPublisher(),
                                         duration: duration, power: power))
    }
    
    func addTextBlinker<T: Publisher>(subscribedTo publisher: T, text: String = "Copied", duration: Double = 0.9)
        -> some View where T.Output == Void, T.Failure == Never {
            
            // here I take whatever publisher we got and type erase it to AnyPublisher
            //   that just simplifies the type so I don't have to add extra generics below
            self.modifier(CopiedBlinker(subscribedTo: publisher.eraseToAnyPublisher(), text: text,
                                         duration: duration))
    }
}

// you could call the .modifier(OpacityBlinker(...)) on your view directly,
//   but I like the View extension method, as it just feels cleaner to me
@available(OSX 11.0, *)
public struct OpacityBlinker: ViewModifier {
    // this is just here to switch on and off, animating the blur on and off
    @State private var isBlurred = false
    var publisher: AnyPublisher<Void, Never>
    // The total time it takes to blur and unblur
    var duration: Double
    
    var power: CGFloat

    // this initializer is not necessary, but allows us to specify a default value for duration,
    //   and the call side looks nicer with the 'subscribedTo' label
    public init(subscribedTo publisher: AnyPublisher<Void, Never>, duration: Double = 1, power: CGFloat) {
        self.publisher = publisher
        self.duration = duration
        self.power = power
    }
    
    public func body(content: Content) -> some View {
        content
            .animationDisable()
            .blur(radius: isBlurred ? power : 0)
            // This basically subscribes to the publisher, and triggers the closure
            //   whenever the publisher fires
            .onReceive(publisher) { _ in
                // perform the first half of the animation by changing isBlurred to true
                // this takes place over half the duration
                withAnimation(.linear(duration: self.duration / 2)) {
                    self.isBlurred = true
                    // schedule isBlurred to return to false after half the duration
                    // this means that the end state will return to an unblurred view
                    DispatchQueue.main.asyncAfter(deadline: .now() + self.duration / 2) {
                        withAnimation(.linear(duration: self.duration / 2)) {
                            self.isBlurred = false
                        }
                    }
                }
        }
    }
}

@available(OSX 11.0, *)
public struct CopiedBlinker: ViewModifier {
    @State private var isDisplayed = false
    var publisher: AnyPublisher<Void, Never>
    var text: String
    var duration: Double
    
    public init(subscribedTo publisher: AnyPublisher<Void, Never>, text: String, duration: Double = 1) {
        self.publisher = publisher
        self.duration = duration
        self.text = text
    }
    
    public func body(content: Content) -> some View {
        ZStack{
            content
                .opacity(isDisplayed ? 0 : 1)
                .onReceive(publisher) { _ in
                    withAnimation(.easeOut(duration: self.duration / 2)) {
                        self.isDisplayed = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + self.duration / 2) {
                            withAnimation(.easeIn(duration: self.duration / 2)) {
                                self.isDisplayed = false
                            }
                        }
                    }
                }
            
            Text(text)
                .opacity(isDisplayed ? 1 : 0)
        }
    }
}
