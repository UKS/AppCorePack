import Foundation
import SwiftUI

@available(OSX 11.0, *)
public extension View {
    func asHiddenTextField(loclizedStr: String?, text: Binding<String>, onEnterPress: @escaping () -> Void) -> some View {
        self.modifier(asHiddenTextFieldModifier(txt: text, loclizedStr: loclizedStr, onEnterPress: onEnterPress) )
    }
}

@available(OSX 11.0, *)
public struct asHiddenTextFieldModifier: ViewModifier {
    @Environment(\.colorScheme) var scheme
    @State var editMode: Bool = false
    @Binding var text : String
    @State var originalText: String
    
    var loclizedStr: String?
    var onEnterPress: () -> Void
    
    public init(txt : Binding<String>, loclizedStr: String? = nil, onEnterPress: @escaping () -> Void) {
        _text = txt
        _originalText = State(initialValue: txt.wrappedValue)
        self.loclizedStr = loclizedStr
        self.onEnterPress = onEnterPress
    }
    
    public func body(content: Content) -> some View {
        ZStack {
            content
                .opacity(editMode ? 0 : 1)
            
            if #available(macOS 12.0, *) {
                TextField(loclizedStr ?? "", text: $text )
                    .onSubmit { editMode = false; onEnterPress() }
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .opacity(editMode ? 1 : 0)
            } else {
                TextField(loclizedStr ?? "", text: $text,
                                      onEditingChanged: { _ in },
                                      onCommit: { editMode = false; onEnterPress() } )
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .opacity(editMode ? 1 : 0)
            }
        }
        .onTapGesture { editMode = true }
        .onExitCommand(perform: { text = originalText; editMode = false } )
    }
}
