import Foundation
import Essentials

internal func SecItem_try(_ id: String, block: () -> OSStatus) -> R<Void> {
    let result = block()
    if result == errSecSuccess {
        return .success(())
    } else {
        return .failure(WTF("\(id) failed: \(result.stringDesription)", code: Int(result)))
    }
}

public extension OSStatus {
    var stringDesription: String {
        SecCopyErrorMessageString(self, nil) as String? ?? ""
    }
}
