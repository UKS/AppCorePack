import Foundation
import SwiftUI

@available(macOS 10.15, *)
public enum Theme {
    case normal(Blender)
    case mode(BlenderInstance, Blender)
}

@available(macOS 10.15, *)
public enum ShapeTheme {
    case normal(Blender)
    case gradient([Blender])
    case gradientMode([Blender], BlenderInstance)
    case gradientMode2([Double], BlenderInstance)
}

@available(OSX 10.15, *)
public extension View {
    func theme( _ theme: Theme) -> some View {
        self.modifier(themeContextModifier(theme))
    }
}
@available(OSX 10.15, *)
struct themeContextModifier: ViewModifier {
    private var theme: Theme
    
    @Environment(\.colorScheme) var colorScheme
    

    init(_ theme: Theme) {
        self.theme = theme
    }
    
    @ViewBuilder
    func body(content: Content) -> some View {
        switch theme {
        case let .normal(blender):
            content
                .foregroundColor(colorScheme.white(for: blender))
                .blendMode(colorScheme.blendMode)
        case let .mode(inst, blender):
            content
                .foregroundColor(inst.white(for: blender))
                .blendMode(inst.mode)
        }
    }
}

@available(macOS 10.15, *)
public extension Shape {
    @ViewBuilder
    func themeFill(colorScheme: ColorScheme, theme: ShapeTheme) -> some View {
        switch theme {
        case let .normal(blender):
            self.fill(colorScheme.white(for: blender))
                .blendMode(colorScheme.blendMode)
        case let .gradient(arr):
            self.fill(LinearGradient(gradient: arr.asGradient(scheme: colorScheme), startPoint: .top, endPoint: .bottom))
                .blendMode(colorScheme.blendMode)
            
        case let .gradientMode(arr, blend):
            self.fill(LinearGradient(gradient: arr.asGradient(blend: blend), startPoint: .top, endPoint: .bottom))
                .blendMode(blend.mode)
            
        case let .gradientMode2(arr, blend):
            self.fill(LinearGradient(gradient: arr.asGradient, startPoint: .top, endPoint: .bottom))
                .blendMode(blend.mode)
        }
    }
}

@available(macOS 10.15, *)
private extension Array where Element == Blender {
    func asGradient(scheme: ColorScheme) -> Gradient {
        return Gradient(colors: self.map { scheme.white(for: $0) })
    }
    
    func asGradient(blend: BlenderInstance) -> Gradient {
        return Gradient(colors: self.map { blend.white(for: $0) })
    }
}

@available(macOS 10.15, *)
private extension Array where Element == Double {
    var asGradient : Gradient {
        return Gradient(colors: self.map { Color.init(white: $0) })
    }
}
