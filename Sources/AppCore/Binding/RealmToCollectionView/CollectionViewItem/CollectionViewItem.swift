//
//  CollectionViewItem.swift
//  AppCore
//
//  Created by Loki on 4/4/19.
//  Copyright © 2019 Loki. All rights reserved.
//

import AppKit

protocol CollectionViewItemProtocol {
    var key         : String            { get set }
    var alias       : String            { get set }
    var image       : NSImage?          { get set }
    var signals     : SignalsService?   { get set }
}

open class CollectionViewItem: NSCollectionViewItem, CollectionViewItemProtocol {
    //CollectionViewItemProtocol
    public var key      : String = ""
    public var alias    : String { get { return textField?.stringValue ?? "" } set { textField?.stringValue = newValue } }
    public var image    : NSImage? { get { return imageView?.image } set { imageView?.image = newValue }}
    public var signals  : SignalsService?
    
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        textField?.actionChannel()
            .map { ($0.objectValue as? String) ?? ""  }
            .onUpdate(context: self) { ctx, text in                ctx.textField?.isEditable = false
                ctx.signals?.send(signal: Signal.Collection.Rename(key: ctx.key, newName: text))
        }
    }
    
    @IBAction func delete(_ sender: Any) {
        signals?.send(signal: Signal.Collection.Delete(key: key))
    }
    
    @IBAction public func rename(_ sender: Any) {
        textField?.isEditable = true
        textField?.becomeFirstResponder()
    }
    
    @IBAction func setImage(_ sender: Any) {
        openImageFile()
    }
    
    func openImageFile() {
        if let url = MacOsDialogs.open(title: "CHOOSE A ICON FILE", conf:[.canChooseFiles], startUrl: nil) {
            signals?.send(signal: Signal.Collection.SetIconManual(key: key, url: url))
        }
    }
}
