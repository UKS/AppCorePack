import Foundation
import SwiftUI
import Quartz
import QuickLook

@available(macOS 11.0, *)
public struct UKSImage: View {
    let url: URL
    let size: CGFloat
    
    @State private var thumbnail: NSImage? = nil
    
    public init(url: URL, size: CGFloat) {
        self.url = url
        self.size = size
    }
    
    public var body: some View {
        if let thumbnail = thumbnail {
            Image(nsImage: thumbnail)
                .resizable()
        } else {
            Image(systemName: "photo") // << any placeholder
                .resizable()
                .onAppear(perform: generateThumbnail) // << here !!
        }
    }
    
    func generateThumbnail() {
        self.thumbnail = url.imgThumbnailAdv(size)
    }
}

fileprivate extension URL {
    func imgThumbnailAdv(_ size: CGFloat) -> NSImage? {
        if let img2 = self.getImgThumbnail(size) {
            return img2
        }
        
        return NSWorkspace.shared.highResIcon(forPath: self.path, resolution: Int(size))
    }
}

extension NSImage{
    var pixelSize: NSSize?{
        if let rep = self.representations.first {
            return NSSize(width: rep.pixelsWide, height: rep.pixelsHigh)
        }
        
        return nil
    }
}

fileprivate extension URL {
    func getImgThumbnail(_ size: CGFloat) -> NSImage? {
        let ref = QLThumbnailCreate ( kCFAllocatorDefault,
                                      self as NSURL,
                                      CGSize(width: size, height: size),
                                      [ kQLThumbnailOptionIconModeKey: false ] as CFDictionary
        )
        
        guard let thumbnail = ref?.takeRetainedValue()
        else { return nil }
        
        if let cgImageRef = QLThumbnailCopyImage(thumbnail) {
            let cgImage = cgImageRef.takeRetainedValue()
            return NSImage(cgImage: cgImage, size: CGSize(width: cgImage.width, height: cgImage.height))
        }
        
        return nil
    }
}

fileprivate extension NSWorkspace {
    func highResIcon(forPath path: String, resolution: Int = 512) -> NSImage? {
        if let rep = self.icon(forFile: path)
            .bestRepresentation(for: NSRect(x: 0, y: 0, width: resolution, height: resolution), context: nil, hints: nil) {
            let image = NSImage(size: rep.size)
            image.addRepresentation(rep)
            return image
        }
        return nil
    }
}
