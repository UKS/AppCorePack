import Foundation

public extension OptionSet where RawValue == UInt32 {
    func with( _ options: Self, on: Bool) -> Self {
        if on {
            return Self(rawValue: self.rawValue | options.rawValue)
        } else {
            return Self(rawValue: self.rawValue & ~options.rawValue)
        }
    }
}
