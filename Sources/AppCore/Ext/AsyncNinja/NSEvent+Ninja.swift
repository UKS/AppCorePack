import Foundation
import AppKit
import AsyncNinja

public extension NSEvent {
    static func localAndGlobalMonitor(matching mask: NSEvent.EventTypeMask) -> Channel<NSEvent,Void> {
        return merge(localMonitor(matching: mask), globalMonitor(matching: mask))
            .mapCompletion() { _ in () }
    }
    
    static func localMonitor(matching mask: NSEvent.EventTypeMask) -> Channel<NSEvent,Void> {
        return producer() { producer in
            let monitor = NSEvent.addLocalMonitorForEvents(matching: mask) { [weak producer] in
                producer?.update($0)
                return $0
            }
            
            producer._asyncNinja_notifyFinalization() {
                AppCore.log(title: "NSEvent.ModifierFlag", msg: "localMonitor Finalization")
                NSEvent.removeMonitor(monitor!)
            }
        }
    }
    
    static func globalMonitor(matching mask: NSEvent.EventTypeMask, cancellationToken: CancellationToken? = nil) -> Channel<NSEvent,Void> {
        return producer(cancellationToken: cancellationToken) { producer in
            let monitor = NSEvent.addGlobalMonitorForEvents(matching: mask) { [weak producer] in
                producer?.update($0)
            }
            
            producer._asyncNinja_notifyFinalization() {
                AppCore.log(title: "NSEvent.ModifierFlag", msg: "globalMonitor Finalization")
                NSEvent.removeMonitor(monitor!)
            }
        }
    }
}
