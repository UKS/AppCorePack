import AppKit

public struct Signal {
    public struct WindowDidClose {
        public let sender : Any?
        public let reason : String
        
        public init(sender: Any?, reason: String) {
            self.sender = sender
            self.reason = reason
        }
    }
    
    public struct DaemonsDidInit {
        public init() { }
    }
    
    
    public struct DateDidChange {
        public let from : Date
        public let to   : Date
        
        public init(from: Date, to: Date) {
            self.from = from
            self.to = to
        }
    }
    
    public struct ColorThemeDidChange {
        public enum Theme: String {
            case Light
            case Dark
        }
        public let theme : Theme
        
        public init() {
            self.theme = ColorThemeDidChange.current
        }
        
        public static var current : Theme {
            let value = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Unspecified"
            return Theme(rawValue: value) ?? .Light
        }
    }
}

public extension Signal {
    struct Collection {
        public struct Create {
            public init() { }
        }
        public struct Delete {
            public let key: String
            public init(key: String) { self.key = key }
        }
        public struct Rename {
            public let key      : String
            public let newName  : String
            public init(key: String, newName: String) { self.key = key; self.newName = newName}
        }
        public struct SetUrl {
            public let key      : String
            public let url      : URL
            public init(key: String, url: URL) { self.key = key; self.url = url}
        }
        public struct SetIconManual {
            public let key      : String
            public let url      : URL
            public init(key: String, url: URL) { self.key = key; self.url = url}
        }
        
        public struct Sort {
            public let descriptors : [NSSortDescriptor]
            public init(descriptors : [NSSortDescriptor]) { self.descriptors = descriptors }
        }
        public struct Filter {
            public let predicate: NSPredicate?
            public init(predicate: NSPredicate?) { self.predicate = predicate }
        }
        public struct DidLoadFirstData {
            public let itemsCount : Int
            public init(itemsCount: Int) {
                self.itemsCount = itemsCount
            }
        }
        public struct ForceReload {
            public init() {}
        }
    }
}
