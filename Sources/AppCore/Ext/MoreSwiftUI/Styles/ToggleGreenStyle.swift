import Foundation
import SwiftUI

@available(macOS 11.0, *)
public struct ToggleGreenStyle: ToggleStyle {
    
    public init() {}
    
    public func makeBody(configuration: Configuration) -> some View {
        HStack {
            configuration.label
            Space(10)
            
            ZStack {
                Rectangle()
                    .theme(.normal(.lvl_4))
                    .cornerRadius(20)
                
                if configuration.isOn {
                    Rectangle()
                        .foregroundColor(.green)
                        .opacity(0.7)
                        .cornerRadius(20)
                }
            }
            .overlay(
                Circle()
                    .foregroundColor(.white)
                    .padding(.all, 3)
                    .overlay(
                        Image(systemName: configuration.isOn ? "checkmark" : "xmark")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .font(Font.title.weight(.black))
                            .frame(width: 8, height: 8, alignment: .center)
                            .foregroundColor(configuration.isOn ? .green : .gray)
                    )
                    .offset(x: configuration.isOn ? 11 : -11, y: 0)
                    .animation(Animation.linear(duration: 0.1))
                
                
            )
            .frame(width: 48, height: 25)
            .onTapGesture { configuration.isOn.toggle() }
        }
    }
    
}
