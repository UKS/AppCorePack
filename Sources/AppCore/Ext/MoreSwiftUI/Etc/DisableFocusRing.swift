import SwiftUI

//Disable focusRing for any TextField in project
public extension NSTextField {
    override var focusRingType: NSFocusRingType {
            get { .none }
            set { }
    }
}
