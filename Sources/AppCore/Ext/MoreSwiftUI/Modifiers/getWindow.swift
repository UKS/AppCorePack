import SwiftUI

// This struct needed to get window from View

@available(OSX 11.0, *)
public extension View {
    func getWindow(_ wnd: Binding<NSWindow?>) -> some View {
        self.background(WindowAccessor(window: wnd))
    }
}

@available(OSX 11.0, *)
public struct WindowAccessor: NSViewRepresentable {
    @Binding var window: NSWindow?
    
    public func makeNSView(context: Context) -> NSView {
        let view = NSView()
        DispatchQueue.main.async {
            self.window = view.window
        }
        return view
    }
    
    public func updateNSView(_ nsView: NSView, context: Context) {}
}

// Usage:
//.getWindow($window)
//.onChange(of: window) { _ in
//    if let wnd = window {
//        wnd.level = .floating
//    }
//}
