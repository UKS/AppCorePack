import Foundation
import SwiftUI

public enum Side: Equatable, Hashable {
    case left
    case right
    case top
    case bottom
    case all
}

@available(macOS 11, *)
public extension View {
    @ViewBuilder
    func padding(sides: [Side], value: CGFloat = 8) -> some View {
        if !sides.contains(.bottom) && !sides.contains(.top) && !sides.contains(.left) && !sides.contains(.right) {
            self
        } else if !sides.contains(.bottom) && !sides.contains(.top) {
            hPadding(sides: sides, value: value)
        } else if !sides.contains(.left) && !sides.contains(.right) {
            vPadding(sides: sides, value: value)
        } else {
            allSidePadding(sides: sides, value: value)
        }
    }
    
    private func hPadding(sides: [Side], value: CGFloat) -> some View {
        HStack(spacing: 0) {
            if sides.contains(.left) {
                Space(value)
            }
            
            self
            
            if sides.contains(.right) {
                Space(value)
            }
        }
    }
    
    private func vPadding(sides: [Side], value: CGFloat) -> some View {
        VStack(spacing: 0) {
            if sides.contains(.top) {
                Space(value)
            }
            
            self
            
            if sides.contains(.bottom) {
                Space(value)
            }
        }
    }
    
    private func allSidePadding(sides: [Side], value: CGFloat) -> some View {
        VStack(spacing: 0) {
            if sides.contains(.all) || sides.contains(.top) {
                Space(value)
            }
            
            HStack(spacing: 0) {
                if sides.contains(.all) || sides.contains(.left) {
                    Space(value)
                }
                
                self
                
                if sides.contains(.all) || sides.contains(.right) {
                    Space(value)
                }
            }
            
            if sides.contains(.all) || sides.contains(.bottom) {
                Space(value)
            }
        }
    }
}
