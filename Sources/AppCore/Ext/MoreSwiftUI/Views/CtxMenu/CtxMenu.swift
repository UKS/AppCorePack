import SwiftUI

@available(OSX 11.0, *)
public struct CtxMenu<Content> : View where Content : View {
    let sfImg: String?
    let lbl: String
    let content: () -> Content
    let img: Image?
    
    public init(img: Image, lbl: String, @ViewBuilder content: @escaping () -> Content) {
        self.sfImg = nil
        self.lbl = lbl
        self.content = content
        self.img = img
    }
    
    public init(sfImg: String, lbl: String, @ViewBuilder content: @escaping () -> Content) {
        self.sfImg = sfImg
        self.lbl = lbl
        self.content = content
        self.img = nil
    }
    
    public var body: some View {
        if let sfImg = sfImg {
            if sfImg.isEmpty {
                Menu( "\(lbl)" ) { content() }
            } else {
                Menu( "\(Image(systemName: sfImg))\t\(lbl)" ) { content() }
            }
            
        }
        else if let img = img {
            Menu( "\(img)\t\(lbl)" ) { content() }
        }
    }
}
