import Foundation
import SwiftUI

@available(macOS 10.15, *)
extension View {
    public func cursor(_ cursor: NSCursor) -> some View {
        self.onHover { inside in
            DispatchQueue.main.async {
                if inside {
                    cursor.push()
                } else {
                    NSCursor.pop()
                }
            }
        }
    }
    
    public func cursorPointHand() -> some View {
        self.cursor(.pointingHand)
    }
}
