import CloudKit
import AsyncNinja

public extension CKContainer {
    func fetch(token: CKServerChangeToken?) -> Channel<CKQueryNotification,CKServerChangeToken> {
        return producer(executor: .serialUnique) { producer in
            let operation = CKFetchNotificationChangesOperation(previousServerChangeToken: token)
            
            operation.notificationChangedBlock = { notification in
                guard let notification = notification as? CKQueryNotification else { return }
                
                producer.update(notification)
            }
            
            operation.fetchNotificationChangesCompletionBlock = { newToken, error in
                if let error = error {
                    log(error: error)
                    producer.fail(error)
                } else if let token = newToken {
                    log(msg: "new CHANGE TOKEN received \(token.debugDescription)")
                    producer.succeed(token)
                } else {
                    producer.fail(AppCoreError.generic(info: "IMPOSIBRU: no error and no token"))
                }
            }
            
            self.add(operation)
        }
    }
}

public extension CKQueryNotification {
    func created() -> CKRecord.ID? {
        switch queryNotificationReason {
        case .recordCreated: return self.recordID
        default: return nil
        }
        
    }
}

public extension iCloudNinjaService {
    private static let changeTokenKey = "AppCore.iCloudNinjaService.changeTokenKey"
    
    static var serverChangeToken: CKServerChangeToken? {
        get {
            guard let data = UserDefaults.standard.value(forKey: changeTokenKey) as? Data else {
                return nil
            }
            
            guard let token = NSKeyedUnarchiver.unarchiveObject(with: data) as? CKServerChangeToken else {
                return nil
            }
            
            return token
        }
        set {
            if let token = newValue {
                let data = NSKeyedArchiver.archivedData(withRootObject: token)
                UserDefaults.standard.set(data, forKey: changeTokenKey)
            } else {
                UserDefaults.standard.removeObject(forKey: changeTokenKey)
            }
        }
    }
}

fileprivate func log(error: Error) {
    AppCore.log(title: "iCloudNinja", error: error)
}

fileprivate func log(msg: String) {
    AppCore.log(title: "iCloudNinja", msg: msg, thread: true)
}
