import Foundation
import SwiftUI

public struct ButtStyle {}

@available(macOS 11.0, *)
public extension ButtStyle {
    struct RectStroke: ButtonStyle {
        
        public init() {}
        
        public func makeBody(configuration: Configuration) -> some View {
            RectHoverButtView(configuration: configuration)
        }
    }
}

@available(macOS 11.0, *)
struct RectHoverButtView : View {
    let configuration: ButtonStyle.Configuration
    
    @State var hover : Bool = false
    
    var body: some View {
        ZStack {
            //if hover {
                configuration.label
                    .padding(1)
                    .background(
                        RoundedRectangle(cornerRadius: 7).stroke().theme(.normal(.lvl_5))
                            .padding(.horizontal, -3)
                            .opacity(hover ? 1 : 0)
                    )
            //} else {
            //    configuration.label.padding(1)
            //}
            
        }.onHover { hover = $0 }
            .animation(.default, value: hover)
    }
}


