import Foundation
import SwiftUI

@available(OSX 11.0, *)
public struct ExecuteCode : View {
    public init( _ codeToExec: () -> () ) {
        codeToExec()
    }
    
    public var body: some View {
        return EmptyView()
    }
}
