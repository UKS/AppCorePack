import Foundation
import SwiftUI

@available(OSX 11.0, *)
public enum SheetDialogType {
    case none
    case view(view: AnyView )
}

@available(OSX 11.0, *)
public extension SheetDialogType {
    /// Does't work as SETter
    /// to hide use .none value
    var isDisplayed: Binding<Bool> { Binding { return !isNone } set: { _ in } }
    
    var isNone: Bool {
        if case .none = self {
            return true
        }
        return false
    }
    
    var isShown: Bool {
        if case .none = self {
            return false
        }
        return true
    }
    
    @ViewBuilder
    func asView() -> some View  {
        switch self {
        case .none:             EmptyView()
        case .view(let view):   view
        }
    }
}

@available(OSX 11.0, *)
public extension View {
    func sheet(sheet: SheetDialogType) -> some View {
        self.modifier( CustomSheetMod(sheetDialogType: sheet ) )
    }
    
    @ViewBuilder
    func sheet2(sheet: SheetDialogType) -> some View {
        if sheet.isDisplayed.wrappedValue {
            ZStack {
                self
                
                VisualEffectView()
                    .onTapGesture { }
                
                sheet.asView()
            }
        } else {
            self
        }
    }
}

@available(OSX 11.0, *)
struct CustomSheetMod: ViewModifier {
    var sheetDialogType: SheetDialogType
    
    public func body (content: Content) -> some View {
            content
                .blur(radius: sheetDialogType.isDisplayed.wrappedValue ? 1.4 : 0)
                .sheet(isPresented: sheetDialogType.isDisplayed) { sheetDialogType.asView() }
    }
}
