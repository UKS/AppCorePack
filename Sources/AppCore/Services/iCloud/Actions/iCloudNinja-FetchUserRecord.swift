import CloudKit
import AsyncNinja

public extension CKContainer {
    func userRecordID() -> Future<CKRecord.ID> {
        return promise() { [weak self] promise in
            self?.fetchUserRecordID { recordID, error in
                if let error = error {
                    promise.fail(error)
                }
                if let id = recordID {
                    AppCore.log(title: "iCloudNinja", msg: "user id did fetch \(id.recordName)")
                    promise.succeed(id)
                }
            }
        }
    }
}
