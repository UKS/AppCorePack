import SwiftUI

@available(OSX 11.0.0, *)
public struct TaoToggle : View {
    public var state : CheckState = CheckState.mixed
    public var action : () -> Void = {}
    @State public var hover : Bool = false
    
    public init( state: CheckState, action: @escaping () -> Void ){
        self.state  = state
        self.action = action
    }
    
    public var body : some View {
        ZStack {
            ZStack {
                if state == .busy {
                    Rectangle().theme(.normal(.lvl_1))
                } else if hover {
                    Rectangle().theme(.normal(.lvl_6))
                } else {
                    Rectangle().theme(.normal(.lvl_2))
                }
                
                Text(state.str)
            }
            .frame(width:18, height: 18)
            .cornerRadius(5)
            .onTapGesture {
                if state != .busy {
                    self.action()
                }
            }
            .onHover { self.hover = $0 }
            
            if (state == .busy) {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle())
                    .scaleEffect(x: 0.6, y: 0.6, anchor: .center)
                    .frame(width:18, height: 18)
            }
        }
    }
}

@available(OSX 11.0.0, *)
public extension TaoToggle {
    enum CheckState {
        case on
        case off
        case mixed
        case busy
        
        public var str : String {
            switch self {
            case .on:       return "✓"
            case .off:      return " "
            case .mixed:    return "■"
            case .busy:     return ""
            }
        }
    }
}
