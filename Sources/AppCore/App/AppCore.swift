/*
 MIT License
 
 Copyright (c) 2014 Sergiy Vynnychenko
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import Swinject
import os.log
import AsyncNinja
import Essentials

public class AppCore {
    public static let env           : ServiceEnvironment = environment()
    public static let scenes        = container.resolve(Scenes.self)!
    public static let signals       : SignalsService = { initSignalSubscribeHandlers(); return SignalsService.main }()
    public static let states        = container.resolve(StatesService.self)!
    public static let bundle        = Bundle.main
    public static var executors     = [String:Executor]()
    
    // Containers
    //public static var mvvmContainer : Container?    // MVVMController resolves ViewModel from this container
    public static let container     = AppCoreContainer(env: env)
    
    public static var logFilters = [String]()
    
    private static var daemons : DaemonsService?
    
    public static var onError : ((String,Error) -> Void)?
}

public extension AppCore {
    static func initDaemonService(signals: SignalsService? = nil, container: Container? = nil) {
        if daemons == nil {
            daemons = DaemonsService(container: container ?? AppCore.container)
        }
    }
}

public extension AppCore {
    static func log(title: String, msg: String, thread: Bool = false) {
        guard shouldPass(title: title) else { return }
        
        
        if thread {
            #if DEBUG
            let thread = Thread.current.dbgName.replace(of: "/Users/loki/dev/", to: "")
                .replace(of: "NSOperationQueue Main Queue", to: "Q MAIN")
            #else
            let thread = Thread.current.dbgName
            #endif
            myPrint("[\(title)]: \(msg) 􀆔\(thread)")
        } else {
            myPrint("[\(title)]: \(msg)")
        }
    }
    
    static func log(title: String, error: Error, thread: Bool = false) {
        onError?(title,error)
        
        guard shouldPass(title: title) else { return }
        if thread {
            myPrint("\n[\(title) ERROR] (\(Thread.current.dbgName)) \(error.detailedDescription_MultiLine)", wrap: true)
        }else {
            myPrint("[\(title) ERROR] \(error.detailedDescription_MultiLine)", wrap: true)
        }
    }
    
    static private func shouldPass(title: String) -> Bool {
        if logFilters.count == 0 {
            return true
        } else {
            return logFilters.contains(title)
        }
    }
    
    static var timestamp : String { return debugDateFormatter.string(from: Date()) }
    
    private static let debugDateFormatter: DateFormatter = { () -> DateFormatter in
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss.SSS"
        return dateFormatter
    }()
}

func myPrint(_ msg: String, wrap: Bool = false) {
    if wrap {
        print("########################################################")
    }
    #if DEBUG
        print("\(AppCore.timestamp) " + msg)
    #else
        os_log("%{public}@", msg)
    #endif
    if wrap {
        print("########################################################")
    }
}


public extension Error {
    var detailedDescription_MultiLine : String {
        var result = ""
        
        result += "\nCODE  : \((self as NSError).code)"
        result += "\nDESC  : " + self.localizedDescription
        
        if let reason = __reason {
            result += "\n\(reason)"
        }
        
        if let domain = __domain {
            result += "\n\(domain)"
        }
        
        if let userInfo = __userInfo {
            result += "\n\(userInfo)"
        }
        
        return result
    }
    
    var detailedDescription : String {
        var result = __code + " " + self.localizedDescription
        
        if let reason = __reason {
            result += " | \(reason)"
        }
        
        if let domain = __domain {
            result += " | \(domain)"
        }
        
        if let userInfo = __userInfo {
            result += " | \(userInfo)"
        }
        
        return result
    }
}

fileprivate extension Error {
    var __reason : String? {
        let nsError = self as NSError
        if let reason = nsError.localizedFailureReason {
            return "REASON: \(reason)"
        }
        return nil
    }
    
    var __code : String {
        let nsError = self as NSError
        return "[code: \(nsError.code)]"
    }
    
    var __domain : String? {
        let nsError = self as NSError
        
        if nsError.domain != "" {
            return "DOMAIN: \(nsError.domain)"
        }
        return nil
    }
    
    var __userInfo : String? {
        let nsError = self as NSError
        
        var userInfo = nsError.userInfo
        userInfo[NSLocalizedDescriptionKey] = nil
        userInfo[NSLocalizedFailureReasonErrorKey] = nil
        if !userInfo.isEmpty {
            return "UserInfo: \(userInfo)"
        }
        
        return nil
    }
}
