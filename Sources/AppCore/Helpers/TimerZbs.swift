import Foundation
import AsyncNinja

public class TimerZbs {
    static var timers = [String:Channel<Void,Void>]()

    public class func start(id: String, interval: TimeInterval, code: @escaping ()->()) {
        stop(id: id)
        timers[id] = makeTimer(interval: interval) { code() }
    }
    
    public class func stop(id: String) {
        if let _ = timers[id] {
            timers[id] = nil
        }
    }
}
