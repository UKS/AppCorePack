import Foundation
import Realm
import RealmSwift
import AsyncNinja

public extension Realm {
    func notifications(executor: Executor = .main) -> Channel<Realm.Notification, Void> {
        return producer(executor: executor) { producer in
            let token = self.observe { event, realm in
                producer.update(event)
            }
            
            producer._asyncNinja_retainUntilFinalization(token)
        }
    }
}

public extension Results {
    func notifications(executor: Executor = .main) -> Channel<RealmCollectionChange<Results<Element>>, Void> {
        return producer(executor: executor) { producer in
            let token = self.observe { event in
                producer.update(event)
            }
            
            producer._asyncNinja_retainUntilFinalization(token)
        }
    }
}
