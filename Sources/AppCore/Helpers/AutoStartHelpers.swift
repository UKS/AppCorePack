import AppKit
import ServiceManagement

// https://theswiftdev.com/2017/10/27/how-to-launch-a-macos-app-at-login/
// https://products.delitestudio.com/start-dockless-apps-at-login-with-app-sandbox-enabled/

public class AutoStartHelper {
    let launcherAppName         : String
    let dbgFakeRun              : Bool
    let hostAppId               : String = Bundle.main.bundleIdentifier!
    
    public init(launcherAppName: String, dbgFakeRun : Bool = false) {
        self.launcherAppName = launcherAppName
        self.dbgFakeRun = dbgFakeRun
    }
    
    
    @objc func terminate() {
        NSApp.terminate(nil)
    }
    
    static func hostAppPath() -> String {
        return Bundle.main.bundleURL
            .deletingLastPathComponent()
            .deletingLastPathComponent()
            .deletingLastPathComponent()
            .deletingLastPathComponent()
            .path
    }
    
    static public func runHostApp() {
        NSWorkspace.shared.launchApplication(hostAppPath())
    }
    
    public func isAppRunning(id: String) -> Bool {
        for app in NSWorkspace.shared.runningApplications {
            guard let bundleId = app.bundleIdentifier else { continue }
            
            if bundleId == id {
                return true
            }
        }
        return false
    }
    
    public func launchAtLogIn(_ enabled: Bool) {
        print("SET LAUNCH AT STARTUP : \(enabled) for \(hostAppId)")
        
        if dbgFakeRun {
            print("Debug Fake Run")
            return
        }

        let url = Bundle.main.bundleURL
            .appendingPathComponent("Contents/Library/LoginItems")
            .appendingPathComponent(launcherAppName)
        
        let result : OSStatus = LSRegisterURL(url as CFURL, false)
        
        print("LSRegisterURL result \(result)")
        
        if !SMLoginItemSetEnabled(hostAppId as CFString, enabled) {
            print("SMLoginItemSetEnabled failed \(hostAppId)")
        }
    }
}
