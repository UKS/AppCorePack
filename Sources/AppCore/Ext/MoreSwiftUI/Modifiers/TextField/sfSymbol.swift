import SwiftUI

@available(OSX 11.0, *)
public extension Text {
    static func sfSymbol( _ sysName: String) -> Text {
        return Text(Image(systemName: sysName))
    }
    
    /// You need to use .fixedSize() manually!!!!
    static func sfIcon( _ sysName: String, size: CGFloat) -> Text {
        return Text(Image(systemName: sysName))
            .font(.system(size: size))
    }
    
    static func sfSymbolAndText(sysName: String, text: String) -> Text {
        return Text("\(sysName.asSfSymbol())\(text)")
    }
}

@available(OSX 11.0, *)
public extension String {
    func asSfSymbol() -> Image {
        Image(systemName: self)
    }
}
