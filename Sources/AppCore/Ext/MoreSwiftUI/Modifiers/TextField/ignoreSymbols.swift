import Foundation
import SwiftUI

@available(OSX 11.0, *)
public extension View {
    ///Trimming whitespaces + remove "\n" symbols
    func fixUrl(string: Binding<String>) -> some View {
        self.modifier( FixUrlLineMod(string: string) )
    }
}

@available(OSX 11.0, *)
public extension TextField {
    func disallowSymbols(symbols: [Character], string: Binding<String>) -> some View {
         self.modifier( IgnoreSymbols(symbols: symbols, string: string)  )
    }
    
    func disallowWhiteSpacesOnEdes(string: Binding<String>) -> some View {
        self.modifier( DisallowWhiteSpacesOnEdes(string: string) )
    }
    
    func allowSymbols(_ symbols: [Character], string: Binding<String>) -> some View {
        self.modifier( AllowSymbols(allowedSymbols: symbols, string: string)  )
   }
}

@available(OSX 11.0, *)
public struct IgnoreSymbols: ViewModifier {
    var symbols: [Character]
    var string: Binding<String>
    
    public func body (content: Content) -> some View
    {
        content.onChange(of: string.wrappedValue) { value in
            var newValue = value
        
            for symbol in symbols {
                newValue = newValue.replace(of: "\(symbol)", to: "")
            }
        
            if value != newValue {
                string.wrappedValue = newValue
            }
        }
    }
}

@available(OSX 11.0, *)
public struct AllowSymbols: ViewModifier {
    var allowedSymbols: [Character]
    var string: Binding<String>
    
    public func body (content: Content) -> some View {
        content.onChange(of: string.wrappedValue) { value in
            var newValue = ""
            
            for char in value {
                if allowedSymbols.contains(char) {
                    newValue.append(char)
                }
            }
        
            if value != newValue {
                string.wrappedValue = newValue
            }
        }
    }
}

@available(OSX 11.0, *)
public struct DisallowWhiteSpacesOnEdes: ViewModifier {
    var string: Binding<String>
    
    public func body (content: Content) -> some View {
        content
            .onChange(of: string.wrappedValue) { value in
                let newValue = string.wrappedValue.trimmingCharacters(in: ["\n"," ", "\t"])
                
                if value != newValue {
                    string.wrappedValue = newValue
                }
            }
    }
}

@available(OSX 11.0, *)
public struct FixUrlLineMod: ViewModifier {
    var string: Binding<String>
    
    public func body (content: Content) -> some View {
        content
            .onChange(of: string.wrappedValue) { value in
                let newValue = string.wrappedValue
                                    .replacingOccurrences(of: "\n", with:"")
                                    .trimmingCharacters(in: [" ", "\t"])
                
                if value != newValue {
                    string.wrappedValue = newValue
                }
            }
    }
}


public enum TaoSymbolsSet: String {
    case Numbers    = "01234567890"
    case CharsLower = "abcdefghijklmnopqrstuvwxyz"
    case CharsUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    
    public static func whiteSpaces() -> [Character] {
        return ["\t","\r","\n"," "]
    }
    
    public static func numbers() -> [Character] {
        return Array(TaoSymbolsSet.Numbers.rawValue)
    }
    
    public static func charsLower() -> [Character] {
        return Array(TaoSymbolsSet.CharsLower.rawValue)
    }
    
    public static func charsUpper() -> [Character] {
        return Array(TaoSymbolsSet.CharsUpper.rawValue)
    }
    
    public static func chars() -> [Character] {
        return charsLower()
            .appending(contentsOf: charsUpper())
    }
    
    public static func main() -> [Character] {
        chars()
            .appending(contentsOf: numbers())
            .appending(contentsOf: Array(":_/.@-"))
    }
    
    public static func folderNameDisallowChars() -> [Character] {
        return [":","\\"]
    }
    
    public static func folderPathDisallowChars() -> [Character] {
        return [":"]
    }
    
    public static func gitIgnoreTagsAllowedChars() -> [Character] {
        chars()
            .appending(contentsOf: ["+"])
    }
}
