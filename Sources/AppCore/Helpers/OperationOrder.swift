import Foundation

public class OperationOrder {
    var lastOperation : Operation?
    
    public func addAsDependentOfPrevious(_ nextOperation: Operation) {
        if let prev = lastOperation {
            nextOperation.addDependency(prev)
        }
    }
}
