//
//  SecurityItem.swift
//  AppCore
//
//  Created by loki on 10.06.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import Foundation
import Essentials

final public class SecurityItem {
    static func add(_ query: [String:Any]) -> R<Void> {
        SecItem_try("SecItemAdd") {
            SecItemAdd(query as CFDictionary, nil)
        }
    }
    
    static func copyMatching(_ query: [String:Any]) -> R<AnyObject> {
        var result: AnyObject?
        
        return SecItem_try("SecItemCopyMatching") {
            SecItemCopyMatching(query as CFDictionary, &result)
        }
        | { result.asNonOptional }
    }
    
    static func update(_ query: [String:Any], _ attributesToUpdate: [String:Any]) -> R<Void> {
        SecItem_try("SecItemUpdate") {
            SecItemUpdate(query as CFDictionary, attributesToUpdate as CFDictionary)
        }
    }
    
    static func delete(_ query: [String:Any]) -> R<Void> {
        SecItem_try("SecItemDelete") {
            SecItemDelete(query as CFDictionary)
        }
    }
}
