import Foundation
import SwiftUI
import AsyncNinja

///////////////////////////////////////////
/// Usage sample
///////////////////////////////////////////
//struct SplitViewTest: View {
//    var body: some View {
//        SplitView(
//            viewId: "splitViewCustom",
//            master: {
//                Text("master")
//                    .frame(maxWidth: .infinity, maxHeight: .infinity)
//                    .background(Color.yellow)
//            }, detail: {
//                Text("Detail")
//                    .frame(maxWidth: .infinity, maxHeight: .infinity)
//                    .background(Color.orange)
//            }
//        )
//    }
//}

@available(macOS 10.15, *)
public struct HHSplit : View {
    let inputViews: [AnyView]
    var defaultPos      : CGFloat   = 300
    let id : String
    
    public init<V0: View, V1: View>(id: String = "", @ViewBuilder content: () -> TupleView<(V0, V1)>) {
        self.id = id
        let cv = content().value
        inputViews = [AnyView(cv.0), AnyView(cv.1)]
    }
    
    public var body: some View {
        let viewControllers = [NSHostingController(rootView: inputViews[0]), NSHostingController(rootView: inputViews[1])]
        return SplitViewRepresentable(viewControllers: viewControllers, id: id, defaultPos: defaultPos, vertical: false)
    }
}

@available(macOS 10.15, *)
public struct VVSplit : View {
    let inputViews: [AnyView]
    var defaultPos      : CGFloat   = 300
    let id : String
    
    public init<V0: View, V1: View>(id: String = "", @ViewBuilder content: () -> TupleView<(V0, V1)>) {
        self.id = id
        let cv = content().value
        inputViews = [AnyView(cv.0), AnyView(cv.1)]
    }
    
    public var body: some View {
        let viewControllers = [NSHostingController(rootView: inputViews[0]), NSHostingController(rootView: inputViews[1])]
        return SplitViewRepresentable(viewControllers: viewControllers, id: id, defaultPos: defaultPos, vertical: true)
    }
}



@available(macOS 10.15, *)
public struct SplitView<Master: View, Detail: View>: View {
    var id              : String
    var defaultPos      : CGFloat   = 300
    var master: Master
    var detail: Detail
        
    public init(@ViewBuilder master: () -> Master, @ViewBuilder detail: () -> Detail, id: String = "") {
        self.master = master()
        self.detail = detail()
        self.id = id
    }
    
    public var body: some View {
        let viewControllers = [NSHostingController(rootView: master), NSHostingController(rootView: detail)]
        return SplitViewRepresentable(viewControllers: viewControllers, id: id, defaultPos: defaultPos, vertical: false)
    }
}

@available(macOS 10.15, *)
public struct SplitViewRepresentable: NSViewControllerRepresentable {
    public typealias NSViewControllerType = NSSplitViewController
    var viewControllers: [NSViewController]
    var id : String
    var defaultPos : CGFloat
    var vertical : Bool
    
    public func makeNSViewController(context: Context) -> NSSplitViewController {
        let controller = SplitViewController()
        controller.children = viewControllers
        controller.defaultPos = defaultPos
        controller.id = id
        controller.splitView.isVertical = !vertical // it works vise versa in old school

        return controller
    }
    
    public func updateNSViewController(_ splitController: NSSplitViewController, context: Context) {
        //print("updateNSViewController")
    }
}


class SplitViewController: NSSplitViewController {
    var id = ""
    var defaultPos : CGFloat = 300
    let posDidUpdate = Producer<CGFloat,Void>()
    let ctx = NinjaContext.Main()
    
    var __appeared = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        splitView.dividerStyle = .thin
        
        posDidUpdate
            .distinct()
            .throttle(interval: 0.3, after: .sendLast)
            .onUpdate(context: ctx) { [weak self] _, pos in
                if let saved = self?.savedPosition {
                    if saved != pos {
                        self?.savedPosition = pos
                    }
                }
            }
    }
    
    override func viewWillAppear() {
        __appeared = true
        position = savedPosition
    }
    
    override func viewWillDisappear() {
        __appeared = false
    }
    
    override func splitViewDidResizeSubviews(_ notification: Notification) {
        guard position > 0 else { return }
        posDidUpdate.update(position)
    }
    
    var position : CGFloat {
        set {
            AppCore.log(title: "Split+" + id, msg: "position.newValue = \(newValue)")
            splitView.setPosition(newValue, ofDividerAt: 0)
        }
        get {
            if !splitView.isVertical {
                return splitView.subviews[0].frame.height
            } else {
                return splitView.subviews[0].frame.width
            }
        }
    }
    
    private var prefix : String  {
        if splitView.isVertical {
            return "split.position.h."
        } else {
            return "split.position.v."
        }
    }
    
    var savedPosition : CGFloat {
        set {
            AppCore.log(title: "Split+" + id, msg: "savedPosition.set = \(newValue)")
            UserDefaults.standard.set(newValue, forKey: prefix + id)
        }
        get {
            let val = (UserDefaults.standard.value(forKey: prefix + id) as? CGFloat) ?? defaultPos
            //AppCore.log(title: "Split+" + id, msg: "savedPosition.get = \(val), default: \(defaultPos)")
            return val
        }
    }
}
