import Foundation
import SwiftUI

@available(macOS 11.0, *)
public extension ButtStyle {
    struct RectSolid: ButtonStyle {
        let padding : CGFloat
        //public init() {}
        public init(padding: CGFloat = 10) {
            self.padding = padding
        }
        
        public func makeBody(configuration: Configuration) -> some View {
            RectSolidButtView(configuration: configuration, padding: padding)
        }
    }
}

@available(macOS 11.0, *)
struct RectSolidButtView : View {
    let configuration: ButtonStyle.Configuration
    let padding : CGFloat
    @State var hover : Bool = false
    
    var body: some View {
        ZStack {
            configuration.label
                .padding(padding)
                .background(BG(hover: $hover, isPressed: configuration.isPressed))
        }.onHover { hover = $0 }
    }
}

@available(macOS 11.0, *)
fileprivate struct BG : View {
    @Binding var hover : Bool
    let isPressed : Bool
    
    var body: some View {
        RoundedRectangle(cornerRadius: 7)
            .fill(Color(hex: 0x00901f))
            .opacity(isPressed ? 0.6 : (hover ? 1 : 0.75))
    }
}

