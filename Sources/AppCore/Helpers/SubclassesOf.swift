import Foundation

public class AllClasses {
    var count: Int32 = 0
    let allClasses : UnsafeMutablePointer<AnyClass>
    
    init() {
        let expectedClassCount = objc_getClassList(nil, 0)
        allClasses = UnsafeMutablePointer<AnyClass>.allocate(capacity: Int(expectedClassCount))
        let autoreleasingAllClasses = AutoreleasingUnsafeMutablePointer<AnyClass>(allClasses)
        count = objc_getClassList(autoreleasingAllClasses, expectedClassCount)
        //allClasses = objc_copyClassList(&count)!
    }
    
    public func subclasses<T>(of theClass: T) -> [T] {
        var result: [T] = []
        let classPtr = address(of: theClass)
        
        for n in 0 ..< count {
            let someClass: AnyClass = allClasses[Int(n)]
            guard let someSuperClass = class_getSuperclass(someClass),
                address(of: someSuperClass) == classPtr
                else { continue }
            
            result.append(someClass as! T)
        }
        
        return result
    }
}

public func subclasses<T>(of theClass: T) -> [T] {
    var count: UInt32 = 0, result: [T] = []
    let allClasses = objc_copyClassList(&count)!
    let classPtr = address(of: theClass)
    
    for n in 0 ..< count {
        let someClass: AnyClass = allClasses[Int(n)]
        guard let someSuperClass = class_getSuperclass(someClass),
            address(of: someSuperClass) == classPtr
            else { continue }
        
        result.append(someClass as! T)
    }
    
    return result
}

private func address(of object: Any?) -> UnsafeMutableRawPointer{
    return Unmanaged.passUnretained(object as AnyObject).toOpaque()
}
