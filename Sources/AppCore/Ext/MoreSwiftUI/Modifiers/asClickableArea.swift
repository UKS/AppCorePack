import SwiftUI
import Combine

@available(OSX 11.0, *)
public extension View {
    func asClickableArea(clickAct: @escaping ()->()) -> some View {
        self.modifier( ClicableAreaMod(clickAct: clickAct ) )
    }
}

@available(OSX 11.0, *)
struct ClicableAreaMod: ViewModifier {
    @State var hovering: Bool = false
    var clickAct: () -> ()
    
    public func body(content: Content) -> some View {
        ZStack {
            RoundedRectangle(cornerRadius: 5)
                .theme(.normal(.lvl_3))
                .animationDisable()
                .opacity(hovering ? 0.2 : 0)
                .animation(.default, value: hovering)
        
            content
                .makeFullyIntaractable()
                .onHover { hovering in self.hovering = hovering }
                .onTapGesture { clickAct() }
                .animationDisable()
        }
    }
}
