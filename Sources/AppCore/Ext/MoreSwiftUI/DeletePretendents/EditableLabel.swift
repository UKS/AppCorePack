//
//  EditableLabel.swift
//  AppCore
//
//  Created by UKS on 07.06.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import Foundation
import SwiftUI

@available(macOS 10.15, *)
public struct EditableLabel: View {
    @Binding var text: String
    @State private var newValue: String = ""

    @State var editProcessGoing = false { didSet{ newValue = text } }

    let onEditEnd: () -> Void

    public init(_ txt: Binding<String>, onEditEnd: @escaping () -> Void) {
        _text = txt
        self.onEditEnd = onEditEnd
    }

    @ViewBuilder
    public var body: some View {
        ZStack {
            Text(text)
                .opacity(editProcessGoing ? 0 : 1)

            TextField("", text: $newValue, onEditingChanged: { _ in }, onCommit: { text = newValue; editProcessGoing = false; onEditEnd() } )
                .opacity(editProcessGoing ? 1 : 0)
        }
        .onTapGesture(count: 1, perform: { editProcessGoing = true } )
        .onExitCommand(perform: { editProcessGoing = false; newValue = text })
    }
}



/// Sample of usage:
///
/// EditableLabel($testText, onEditEnd: { print("new name is *\(testText)* ") } )

// use .asHiddenTextField instead
