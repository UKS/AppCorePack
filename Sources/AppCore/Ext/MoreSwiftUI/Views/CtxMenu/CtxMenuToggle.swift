import SwiftUI

@available(OSX 11.0, *)
public struct CtxMenuToggle: View {
    let isOn: Binding<Bool>
    let lbl: String
    
    public init(isOn: Binding<Bool>, lbl: String) {
        self.isOn = isOn
            
        self.lbl = lbl
    }
    
    public var body: some View {
        Button(action: { isOn.wrappedValue.toggle() } ) {
            HStack(spacing: 0) {
                Text(isOn.wrappedValue ? "✓" : "")
                +
                Text("\t\(lbl)")
            }
        }
    }

}
