import Foundation
import SwiftUI

@available(OSX 11.0, *)
public extension View {
    func dragAs(string: String, addSpaceBeginEnd: Bool = true) -> some View  {
        self
            .onDrag { return NSItemProvider(object: String("\(addSpaceBeginEnd ? " " : "")\(string)\(addSpaceBeginEnd ? " " : "")") as NSString ) }
            .cursor(.openHand)
    }
}
