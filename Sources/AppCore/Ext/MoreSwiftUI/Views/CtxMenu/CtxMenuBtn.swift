import SwiftUI
import Essentials

@available(OSX 11.0, *)
public struct CtxMenuBtn: View {
    let action: () -> Void
    let sfImg: String?
    let lbl: String
    
    var lblWrapper: String { lbl == "" ? lbl : "\t\(lbl)" }
    
    let color: Color?
    
    let img: Image?
    
    public init(img: Image, lbl: String, maxLength: Int = 70, color: Color? = nil, action: @escaping () -> Void ){
        self.action = action
        self.sfImg = nil
        self.lbl = lbl.truncCenter(length: maxLength)
        self.color = color
        self.img = img
    }
    
    public init(sfImg: String, lbl: String, maxLength: Int = 70, color: Color? = nil, action: @escaping () -> Void ){
        self.action = action
        self.sfImg = sfImg
        self.lbl = lbl.truncCenter(length: maxLength)
        self.color = color
        self.img = nil
    }
    
    public var body: some View {
        if let img = img {
            Button(action: action ) {
                Text("\(img)\(lblWrapper)")
                    .if(color != nil) { $0.foregroundColor(color!)}
            }
        }
        else if let icon = sfImg,
           icon.count <= 1 {
            Button(action: action ) {
                Text("\(icon)\(lblWrapper)")
                    .if(color != nil) { $0.foregroundColor(color!)}
            }
        }
        else if let icon = sfImg {
            Button(action: action ) {
                Text.sfSymbolAndText(sysName: icon, text: "\(lblWrapper)")
                    .if(color != nil) { $0.foregroundColor(color!) }
            }
        }
    }
}
