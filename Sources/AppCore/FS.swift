import Foundation
import AsyncNinja
import Essentials
import AppKit

public class FSApp {
    public static var fmDefault: FileManager { FileManager.default }
    
    public static var home : URL { return fmDefault.homeDirectoryForCurrentUser }
    
    public static var executableUrl: URL? {
        Bundle.main
            .executablePath?
            .asURL()
            .deletingLastPathComponent()
            .deletingLastPathComponent()
            .deletingLastPathComponent()
    }
    
    public class func appFolder() -> URL {
        #if os(iOS)
        return fmDefault.urls(for: .documentDirectory, in: .userDomainMask).first!
        #elseif os(OSX)
        return try! applicationSupportFolderURL().get()
        #endif
    }
    
    public class func urlFor(file: String) -> URL {
        return appFolder().appendingPathComponent(file)
    }
    
    public class func resourcePath(_ file: String, ofType: String) -> String? {
        return Bundle.main.path(forResource: file, ofType: ofType)
    }
    
    public class func resourceURL(_ file: String, ofType: String) -> URL? {
        return Bundle.main.url(forResource: file, withExtension: ofType)
    }
    
    public class func appSupportRoot(in mask: FileManager.SearchPathDomainMask) -> URL {
        do {
            return try fmDefault.url(for: FileManager.SearchPathDirectory.applicationSupportDirectory, in: mask, appropriateFor: nil, create: false)
        } catch {
            fatalError()
        }
    }
    
    public class func deleteFilesWith(prefix: String, at: URL? = nil) {
        let location = at ?? appFolder()
        let urls = location.getFiles()
        
        for url in urls {
            if url.lastPathComponent.hasPrefix(prefix) {
                FS.delete(url)
                    .onFailure {
                        print("Failed to delete file: \(url.path). \nError:\($0)" )
                    }
            }
        }
    }
}

extension FSApp {
    public class func applicationSupportFolderURL(in mask: FileManager.SearchPathDomainMask = .userDomainMask) -> R<URL> {
        return Result {
            try fmDefault.url(for: FileManager.SearchPathDirectory.applicationSupportDirectory, in: mask, appropriateFor: nil, create: true)
        }
        .map { $0.appendingPathComponent(Bundle.main.bundleIdentifier!) }
        .flatMap { fullPath in
            Result {
                try fmDefault.createDirectory(at: fullPath, withIntermediateDirectories: true, attributes: nil)
            }
            .map{ _ in fullPath }
        }
    }
    
    public class func applicationSupportFolder() -> String {
        return try! applicationSupportFolderURL().get().absoluteString
    }
}

public class FS {
    public static var fmDefault: FileManager { FileManager.default }
    
    public class func exist(_ url: URL?) -> Bool {
        guard let url = url else { return false }
        
        return exist(url.path)
    }
    
    public class func exist(_ path: String) -> Bool {
        return fmDefault.fileExists(atPath: path)
    }
    
    @discardableResult
    public class func mkdir(_ url : URL) -> R<()> {
        mkdir(url.path)
    }
    
    @discardableResult
    public class func mkdir(_ path : String) -> R<()> {
        if exist(path) {
            return .wtf("Dir already exist: \(path)")
        }
        
        return Result {
            try fmDefault.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
        .flatMapError{ .wtf("Can't create dir at path : \(path). \nError: \($0.localizedDescription)")}
    }
    
    public class func copy(_ fromUrl: URL, toUrl: URL, replace: Bool = false) -> R<()> {
        copy(fromUrl.path, toPath: toUrl.path, replace: replace)
    }
    
    public class func copy(_ fromPath: String, toPath: String, replace: Bool = false) -> R<()> {
        var result: R<()> = .success(())
        
        // delete destination file if it exist
        if replace && fmDefault.fileExists(atPath: toPath) {
            result = delete(toPath)
        }
        
        return result.flatMap { Result { try fmDefault.copyItem(atPath: fromPath, toPath: toPath) } }
    }
    
    public class func move(_ fromUrl: URL, toUrl: URL, replace: Bool = false) -> R<()> {
        move(fromUrl.path, toPath: toUrl.path, replace: replace)
    }
    
    public class func move(_ fromPath: String, toPath: String, replace: Bool = false) -> R<()> {
        var result: R<()> = .success(())
        
        if replace && exist(toPath) {
            result = delete(toPath)
        }
        
        return result.flatMap { Result { try fmDefault.moveItem(atPath: fromPath, toPath: toPath) } }
    }
    
    public class func delete(_ url: URL) -> R<()> { delete(url.path) }
    
    public class func delete(_ path: String) -> R<()> {
        Result { try fmDefault.removeItem(atPath: path) }
    }
    
    /// Returns new URL from trash
    @discardableResult
    public class func deleteToTrash(_ url : URL) -> R<()> {
        let deletedObj: AutoreleasingUnsafeMutablePointer<NSURL?>? = nil
        
        return ( try? fmDefault.trashItem(at: url, resultingItemURL: deletedObj ) )
            .asNonOptional
//            .flatMap { _ in  deletedObj.asNonOptional }
//            .flatMap { $0.pointee.asNonOptional }
//            .map { $0 as URL }
    }
    
    /// Returns new URL from trash
    @discardableResult
    public class func deleteToTrash(_ path : String) -> R<()> {
        deleteToTrash(URL(fileURLWithPath: path))
    }
    
    ///If you put folder's url - it will show in Finder content of this folder. |
    ///If you put file's url - it will show in Finder file's parent and select file there. |
    ///Will do nothing in case url is nil. |
    ///Will do nothing in case file/path does not exist.
    public class func showInFinder(url: URL?) {
        showInFinder(url: url, selectLastComponent: !(url?.isDirectory ?? true) )
    }
    
    public class func showInFinder(url: URL?, selectLastComponent: Bool) {
        guard let url = url else { return }
        
        if selectLastComponent {
            NSWorkspace.shared.activateFileViewerSelecting([url])
        } else {
            NSWorkspace.shared.selectFile(nil, inFileViewerRootedAtPath: url.path)
        }
    }
    
    /// returns all of URLs from children folders + of all subdirs files
    public class func contentOfDirectory(url: URL, includingSubdir: Bool) -> R<[URL]> {
        if includingSubdir {
            return .success( url.getFiles() )
        }
        
        return Result {
            try fmDefault
                        .contentsOfDirectory(at: url,
                                             includingPropertiesForKeys: nil,
                                             options: [.skipsHiddenFiles])
        }
    }
    
    ///Returns DIRECT CHILDS-FOLDERS
    public class func subDirectories(of url: URL) -> R<[URL]> {
        Result {
            try fmDefault
                        .contentsOfDirectory(at: url,
                                             includingPropertiesForKeys: nil,
                                             options: [.skipsHiddenFiles])
                        .filter(\.hasDirectoryPath)
        }
    }
    
    public class func openWithAssociatedApp(_ url: URL?) {
        if let url = url {
            openWithAssociatedApp(url.path)
        }
    }
    
    public class func openWithAssociatedApp(_ path: String) { NSWorkspace.shared.openFile(path) }
    
    @available(macOS 10.15, *)
    public class func openTerminal(at url: URL?) {
        guard let url = url,
              let appUrl = NSWorkspace.shared.urlForApplication(withBundleIdentifier: "com.apple.Terminal")
        else { return }
        
        NSWorkspace.shared.open([url], withApplicationAt: appUrl, configuration: NSWorkspace.OpenConfiguration() )
    }
    
    @available(macOS 10.15, iOS 9.0, *)
    public class func openUrlWithApp(_ url: URL, appUrl: URL) {
        openUrlWithApp([url], appUrl:appUrl)
    }
    
    @available(macOS 10.15, iOS 9.0, *)
    public class func openUrlWithApp(_ urls: [URL], appUrl: URL) {
        NSWorkspace.shared.open(urls, withApplicationAt: appUrl, configuration: NSWorkspace.OpenConfiguration())
    }
}
