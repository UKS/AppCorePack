import Foundation

public extension BinaryInteger {
    var decimalDigitsCount : Int {
        return Int(log10(Double(self))) + 1
    }
}
