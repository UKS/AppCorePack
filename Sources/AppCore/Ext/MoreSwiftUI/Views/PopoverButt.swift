import SwiftUI

@available(macOS 10.15, *)
public struct PopoverButt<Label: View, Content: View>: View {
    @ViewBuilder var content: () -> Content
    @ViewBuilder var label: () -> Label
    //@State var showingPopover = false
    @ObservedObject var model : PopoverButtModel
    
    public init(id: String = "", @ViewBuilder _ label: @escaping () -> Label,
                @ViewBuilder content: @escaping () -> Content) {
        self.label = label
        self.content = content
        self.model = PopoverButtModel(id: id)
    }
    
    public init(id: String = "", @ViewBuilder content: @escaping () -> Content) where Label == EmptyView {
        self.label = { EmptyView() }
        self.content = content
        self.model = PopoverButtModel(id: id)
    }
    
    public var body: some View {
        Button(action:{ self.model.showingPopover.toggle() }, label: label)
            .popover(isPresented: $model.showingPopover, content: self.content)
            //.cursorPointHand()
    }
}

@available(macOS 10.15, *)
class PopoverButtModel : NinjaContext.Main, ObservableObject {
    let id : String
    @Published var showingPopover = false
    
    init(id: String) {
        self.id = id
        
        super.init()
        
        AppCore.signals.subscribeFor(Signal.PopoverDismiss.self)
            .filter(context: self) { me, signal in me.id == signal.id }
            .onUpdate(context: self) { me, _ in
                me.showingPopover = false
            }
    }
}

public extension Signal {
    struct PopoverDismiss {
        let id : String
        public init(id: String) {
            self.id = id
        }
    }
}
